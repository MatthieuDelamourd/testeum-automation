package squashTA.resources.seleniumTests.java;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import squashTA.resources.seleniumTests.java.pages.TesteumLoginPage;
import squashTA.resources.seleniumTests.java.pages.TesteumTesterDashboardPage;
import squashTA.resources.seleniumTests.java.utils.AbstractTest;

public class TST008 extends AbstractTest {
	@Test
	public void test_AffichageAccueil() {
		/**********************User Story*************************/
		/*	-EN TANT QUE : Testeur								 */
		String login = "tester1" ;
		String password = "azerty";

		/*	-JE VEUX : Pouvoir m’identifier sur la plateforme	 */
		TesteumLoginPage testeumLoginPage = new TesteumLoginPage(this);
		testeumLoginPage.login(login, password);
		TesteumTesterDashboardPage testeumTesterDashboardPage = new TesteumTesterDashboardPage(this);

		/*	-AFIN DE : Pouvoir accéder aux services				 */
		Assertions.assertTrue(driver.findElement(testeumTesterDashboardPage.HI_USER_TEXT_FIELD).getText().contains("Hi mouloud,"));
		/*********************************************************/
		takeScreenShot();
	}
}