package squashTA.resources.seleniumTests.java.utils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.fail;

public class AbstractTest extends Utils {
	protected static ThreadLocal<RemoteWebDriver> dr = new ThreadLocal<RemoteWebDriver>();
	protected String browser = System.getProperty("env.browser");
	protected String baseUrl = System.getProperty("env.baseurl");
	protected String logConf = System.getProperty("log4j.configurationFile");
	protected String env = System.getProperty("os.name");
	protected String isRemote = "false";
	protected final File chromeDriverFile = new File("webdrivers/chromedriver"); // enlever le exe avant de commiter !
	// pareil pour le port, à mettre en 9090 (port utilise par Jenkins pour Selenium Grid).
	public Integer port = 4444;
	final static Logger logger = Logger.getLogger("logger");

    public WebDriver getWebDriver() {
        return driver;
    }
    
   	// @Rule
	//public ScreenshotIfFailed regleFinEchec = new ScreenshotIfFailed();
    
    @BeforeEach
    public void setUp() throws Exception {
		System.out.println("Browser : " + browser);
		System.out.println("Base URL : " + baseUrl);
		System.out.println("Logs : " + logConf);

	   	if (browser.equals("")) {
			browser = "chrome";
		}
		if (baseUrl.equals("")) {
			baseUrl = "http://www.testeum.com:8080";
		}

		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized");

		if (browser.equals("chrome")) {
			if (env.contains("Windows")) {
				System.out.println("Environnement Windows : " + env);
				System.setProperty("webdriver.chrome.driver", "webdrivers/chromedriver.exe");
				driver = new ChromeDriver();
				driver.manage().window().maximize();
			} else if (env.contains("Mac")) {
				System.out.println("Environnement Mac : " + env);
				System.setProperty("webdriver.chrome.driver", "webdrivers/chromedriverMac");
				driver = new ChromeDriver();
			} else {
				System.out.println("Environnement : " + env);
				System.setProperty("webdriver.chrome.driver", "webdrivers/chromedriver");
				options.AddArgument("--headless");
				options.AddArgument("--whitelisted-ips");
				options.AddArgument("--no-sandbox");
				options.AddArgument("--disable-extensions");
				driver = new ChromeDriver(options);
			}
		} else {
			System.out.println("Le navigateur " + browser + " n'existe pas");
		}

		System.out.println("Ouverture d'un navigateur local.");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("Timeout configure à 30 secondes.");
		driver.manage().window().maximize();
		System.out.println("Taille du navigateur maximisee.");
		driver.navigate().to(baseUrl);
    }

    public void goTo(String url) {
        driver.get(url);
    }

    @AfterEach
    public void tearDown() throws Exception {
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail("Test echoue" + verificationErrorString);
        }

		this.getWebDriver().quit();
    }
    
	private class ScreenshotIfFailed extends TestWatcher {
		@Override
		protected void failed(Throwable e, Description description) {
			System.out.println("Test echoue");
			takeScreenShot(className + "_", "Echec du test " + className);
			driver.quit();
	        dr.set(null);
		}
		@Override
		protected void succeeded(Description description) {
			System.out.println("Test " + description.getDisplayName() + " reussi.\n\n");
			driver.quit();
	        dr.set(null);
		}
	}
	
}