package squashTA.resources.seleniumTests.java.utils;

import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxProfile;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class Utils {
	protected FirefoxBinary binary;
	protected FirefoxProfile profile;
	protected boolean acceptNextAlert = true;
	protected StringBuffer verificationErrors = new StringBuffer();
	protected WebDriver driver;
	protected String browser = System.getProperty("java.vm.specification.vendor");
	protected boolean isRemote = true;
	protected String cheminFichierProperties = "test.properties";
	protected String defaultDataBase;
	protected String defaultDataBaseLogin;
	protected String defaultDataBasePassword;
	protected String baseUrl = System.getProperty("java.vendor.url");
	protected String testName = "";
	protected String className = "";
	protected int timeOut = 5;

	@Rule
	public TestWatcher watcher = new TestWatcher() {
		public void starting(Description description) {
			testName = description.getMethodName();
			className = description.getClassName();
			System.out.println("\n\nDebut du test : " + testName + " dans la classe " + className + "\n\n");
		}
	};

	protected enum DatabaseAction {
		DELETE, INSERT, SELECT, UPDATE
	}

	// Gestion du temps

	protected void sleep(int seconds) {
		System.out.println("Attente de " + seconds + " seconde(s).");
		try {
			Thread.sleep((long) seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
	}

	protected void sleepms(int milliseconds) throws InterruptedException{
		System.out.println("Attente de " + milliseconds + " milliseconde(s).");
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw e;
		}
	}

	// Gestion de fichiers

	protected static ArrayList<String> getDataFromTextFile(String cheminFichier) {
		ArrayList<String> donneesFichier = new ArrayList<String>();
		Scanner scanner;
		try {
			scanner = new Scanner(new File(cheminFichier).getAbsoluteFile());
			while (scanner.hasNext()) {
				donneesFichier.add(scanner.next());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return donneesFichier;
	}

	protected static ArrayList<Object[]> getTestCasesFromFile(String cheminFichier) {
		ArrayList<String> parametre = getDataFromTextFile(cheminFichier);
		ArrayList<Object[]> casDeTest = new ArrayList<Object[]>();
		for (int i = 0; i < parametre.size(); i++) {
			casDeTest.add(new Object[] { parametre.get(i) });
		}
		return casDeTest;
	}

	protected String getPropValues(String property) {
		System.out.println("Recuperation de la propriete " + property + ".");
		return getPropValues("src/squashTA/targets/chemin-a-remplacer.properties", property);
	}

	protected String getPropValues(String cheminProperties, String property) {
		System.out.println("Recuperation de la propriete " + property + " dans le fichier " + cheminProperties + ".");
		InputStream inputStream = null;
		String propertyDuFicher = "";
		try {
			Properties prop = new Properties();
			String propFileName = cheminProperties;
			inputStream = new FileInputStream(propFileName);
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
			propertyDuFicher = prop.getProperty(property);
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return propertyDuFicher;
	}

	// Gestion de base de donnee

	protected ResultSet sendQueryToDefaultDatabase(String request, DatabaseAction action) {
		System.out.println("Envoi de la requête " + request + ".");
		defaultDataBase = getPropValues("squashtest.ta.database.url");
		defaultDataBaseLogin = getPropValues("squashtest.ta.database.username");
		defaultDataBasePassword = getPropValues("squashtest.ta.database.password");
		return sendQueryToDataBase(defaultDataBase, defaultDataBaseLogin, defaultDataBasePassword, request, action);
	}

	protected ResultSet sendQueryToDataBase(String url, String login, String password, String request,
			DatabaseAction action) {
		System.out.println("Envoi de la requête " + request + ".");
		String resultat = "";
		ResultSet resultSet = null;
		try {
			Class.forName("org.postgresql.Driver");
			Connection connexion = DriverManager.getConnection(url, login, password);
			Statement statement = connexion.createStatement();
			switch (action) {
			case DELETE:
				statement.executeUpdate(request);
				break;
			case INSERT:
				statement.executeUpdate(request);
				break;
			case SELECT:
				resultSet = statement.executeQuery(request);
				break;
			case UPDATE:
				statement.executeUpdate(request);
				break;
			default:
				break;
			}
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return resultSet;
	}

	// Dates

	protected String getDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		String date = sdf.format(new Date());
		return date;
	}

	protected String getDateBeautiful() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy - hh:mm:ss");
		String date = sdf.format(new Date());
		return date;
	}

	// Gestion des screenshots

	protected void takeScreenShot() {
		takeScreenShot(className + "_", "");
	}

	protected void takeScreenShot(String text) {
		takeScreenShot(className + "_", text);
	}

	protected void takeScreenShot(String nomScreenshot, String text) {
		System.out.println("1");
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		System.out.println("2");
		BufferedImage image = null;
		try {
			image = ImageIO.read(scrFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("3");
		int width = image.getWidth();
		int height = image.getHeight();
		Graphics graphics = image.getGraphics();
		System.out.println("4");
		graphics.setFont(graphics.getFont().deriveFont(30f));
		graphics.setColor(Color.BLACK);
		graphics.drawString(getDateBeautiful() + " - " + className, 50, height - 80);
		graphics.drawString(text, 50, height - 50);
		graphics.dispose();
		System.out.println("5");
		try {
			ImageIO.write(image, "png", new File("screenshots/" + nomScreenshot + getDate() + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void clickElement(By el) throws Exception {
		try {
			driver.findElement(el).click();
		} catch	(Exception e) {
			System.out.println("ERROR - element not found - Stacktrace : " + e);
			throw(e) ;
		}
	}

}
