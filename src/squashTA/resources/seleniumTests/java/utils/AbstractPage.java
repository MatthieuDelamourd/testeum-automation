package squashTA.resources.seleniumTests.java.utils;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.*;


public abstract class AbstractPage extends squashTA.resources.seleniumTests.java.utils.Utils {
	
    public AbstractPage(AbstractTest test) {
        super();
        driver = test.getWebDriver();
        assertPage();
        presentPage();
    }

    public AbstractPage(AbstractPage page) {
        super();
        driver = page.getWebDriver();
        assertPage();
        presentPage();
    }

    public void presentPage() {
    	System.out.println("Titre de la page : " + driver.getTitle());
    }

    protected WebDriver getWebDriver() {
        return driver;
    }

    protected abstract void assertPage();

	/*
	 * Gestion des formulaires
	 */

    public void setInputValue(By by, String text) {
    	System.out.println("Envoi du texte " + text + " à l'element " + by.toString());
        WebElement element = driver.findElement(by);
        element.sendKeys(Keys.DELETE);
        element.sendKeys(text);
    }

	/*
	 * Gestions des clics
	 */

    protected void clickElement(By by) {
    	System.out.println("Clic de l'element " + by.toString());
        assertElementPresent(by);
        driver.findElement(by).click();
    }

    /*
     * Attentes
     */

    public void waitUntilTextDisplayed(String text) throws InterruptedException {
    	System.out.println("Texte attendu : " + text);
        boolean isExpectedTextDisplayed = isTextDisplayed(text);
        int compteur = 0;
        while(!isExpectedTextDisplayed && (compteur < timeOut)) {
            isExpectedTextDisplayed = isTextDisplayed(text);
            sleep(1);
            compteur++;
        }
        if(!isExpectedTextDisplayed) {
        	System.out.println("Le texte " + text + " n'a pas ete trouve au bout de " + timeOut + " secondes.");
            assertTextDisplayedNow(text);
        }
    }

    public void waitUntilTextNotDisplayed(String text){
    	System.out.println("En attente de la disparition du texte : " + text);
        boolean isExpectedTextDisplayed = isTextDisplayed(text);
        int compteur = 0;
        while(isExpectedTextDisplayed && compteur < timeOut) {
            isExpectedTextDisplayed = isTextDisplayed(text);
            sleep(1);
            compteur++;
        }
        if(isExpectedTextDisplayed){
        	System.out.println("Le texte " + text + " n'a pas disparu au bout de " + timeOut + " secondes. Nouvelle tentative.");
        }
        assertTextNotDisplayed(text);
    }

	/*
	 * Gestion de l'affichage de la page
	 */

    public void scrollJusquenBas() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript(
                "window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
    }

	/*
	 * Verifications d'affichage
	 */

        // Textes

    public void assertElementContainsText(By by, String text){
        Assertions.assertTrue(getElementText(by).contains(text));
    }

    public void assertTextDisplayedNow(String text) {
    	String errorMessage = "Le texte " + text + " n'a pas ete trouve sur la page " + driver.getTitle() + ".";
    	System.out.println("Texte cherche : " + text);
        if(!isTextDisplayed(text)){
        	takeScreenShot(errorMessage);
        }
        Assertions.assertEquals(errorMessage, isTextDisplayed(text));
    }

    public void assertTextNotDisplayedNow(String text) {
    	String errorMessage = "Le texte " + text + " a ete trouve sur la page " + driver.getTitle() + ".";
    	System.out.println("Texte cherche : " + text);
        if(isTextDisplayed(text)){
        	takeScreenShot(errorMessage);
        }
        Assertions.assertEquals(errorMessage, !isTextDisplayed(text));
    }

    public void assertTextDisplayed(String text) {
    	System.out.println("Texte cherche : " + text);
        boolean isExpectedTextDisplayed = isTextDisplayed(text);
        int compteur = 0;
        while(!isExpectedTextDisplayed && (compteur < timeOut)) {
            isExpectedTextDisplayed = isTextDisplayed(text);
            sleep(1);
            compteur++;
        }
        assertTextDisplayedNow(text);
    }

    public void assertTextNotDisplayed(String text) {
    	System.out.println("Texte cherche : " + text);
        boolean isExpectedTextDisplayed = true;
        int compteur = 0;
        while(isExpectedTextDisplayed && compteur < timeOut) {
            isExpectedTextDisplayed = isTextDisplayed(text);
            sleep(1);
            compteur++;
        }
        assertTextNotDisplayedNow(text);
    }

        // Elements

    protected void assertElementDisplayed(By by) {
        assertElementPresent(by);
        if (!isElementDisplayed(by)) {
        	System.out.println("Scroll jusqu'en bas de la page pour afficher l'element eventuellement masque.");
            scrollJusquenBas();
            Assertions.assertTrue(isElementDisplayed(by));
        }
    }

    protected void assertElementDisplayedNow(By by) {
        Assertions.assertTrue(driver.findElement(by).isDisplayed());
    }

    protected void assertElementPresent(By by) {
        Assertions.assertTrue(isElementPresent(by));
    }


    /*
     * Infos sur la page
     */

    protected String getElementText(By by){
        return driver.findElement(by).getText();
    }

    protected boolean isTextDisplayed(String text){
        return driver.findElement(By.cssSelector("BODY")).getText().contains(text);
    }

    protected boolean isElementPresent(By by) {
    	System.out.println("Recherche de l'element " + by.toString());
        boolean isElementPresent = false;
        int time = 0;
        while (isElementPresent == false && time < timeOut) {
            if (driver.findElement(by) != null) {
                isElementPresent = true;
                System.out.println("Element trouve");
            } else {
                isElementPresent = false;
                System.out.println("Element non trouve");
            }
            sleep(1);
            time++;
        }
        if (isElementPresent == true) {
        	System.out.println("Element " + by.toString() + " trouve à l'essai n°" + time);
        } else {
        	System.out.println("Element " + by.toString() + " non trouve.");
        }
        return isElementPresent;
    }

    protected boolean isElementDisplayed(By by) {
        boolean isElementDisplayed = false;
        if (driver.findElement(by).isDisplayed()) {
            isElementDisplayed = true;
        } else {
            isElementDisplayed = false;
        }
        if (isElementDisplayed == true) {
        	System.out.println("Element " + by.toString() + " correctement affiche.");
        } else {
        	System.out.println("Element " + by.toString() + " non affiche.");
        }
        return isElementDisplayed;
    }

    protected boolean isElementDisplayedNow(By by) {
        return driver.findElement(by).isDisplayed();
    }
}
