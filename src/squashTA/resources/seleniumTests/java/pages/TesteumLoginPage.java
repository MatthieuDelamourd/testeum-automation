package squashTA.resources.seleniumTests.java.pages;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import squashTA.resources.seleniumTests.java.utils.AbstractPage;
import squashTA.resources.seleniumTests.java.utils.AbstractTest;
public class TesteumLoginPage extends AbstractPage {

	private static final By INPUT_LOGIN = By.xpath("//input[@id='username']");
	private static final By INPUT_PASSWORD = By.xpath("//input[@id='password']");
	private static final By CHECKBOX_RETAIN = By.xpath("//input[@id='rememberMe']");
	private static final By LISTE_LANGUAGE = By.cssSelector(".dropdown");
	private static final By LISTE_LANGUAGE_VALUE = By.cssSelector(".dropdown span");
	private static final By LISTE_LANGUAGE_EN = By.cssSelector(".dropdown-item[value='en']");
	private static final By LISTE_LANGUAGE_FR = By.cssSelector(".dropdown-item[value='fr']");
	private static final By BUTTON_CONNECTION = By.cssSelector("button[type=submit]");
	private static final By BUTTON_REGISTER = By.cssSelector("button:contains('Créer un compte')");
	private static final By BUTTON_FORGOT_PWD = By.cssSelector("a:contains('Mot de passe oublié ?')");
	private static final By CHECK_ELEMENT_1 = INPUT_LOGIN;
	private static final By CHECK_ELEMENT_2 = INPUT_PASSWORD;
	
	public TesteumLoginPage(AbstractTest test) { super(test); }
	
	public TesteumLoginPage(AbstractPage page) {
		super(page);
	}

	@Override
	protected void assertPage() {
		this.assertElementDisplayed(CHECK_ELEMENT_1);
		this.assertElementDisplayed(CHECK_ELEMENT_2);

		selectLangage("fr");
	}

	protected void selectLangage(String lang) {
		String currentLang = this.driver.findElement(LISTE_LANGUAGE_VALUE).getText().trim();
		if ((lang.equals("en") && currentLang.equals("FRANCAIS")) || (lang.equals("fr") && currentLang.equals("ENGLISH"))) {

			this.driver.findElement(LISTE_LANGUAGE).click();
			if (lang.equals("en")) {
				this.driver.findElement(LISTE_LANGUAGE_EN).click();
			} else if (lang.equals("fr")) {
				this.driver.findElement(LISTE_LANGUAGE_FR).click();
			}
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			try {
				if (lang.equals("fr")) {
					Assertions.assertTrue(this.driver.findElement(LISTE_LANGUAGE_VALUE).getText().trim().equals("FRANÇAIS"));
				} else if (lang.equals("en")) {
					Assertions.assertTrue(this.driver.findElement(LISTE_LANGUAGE_VALUE).getText().trim().equals("ENGLISH"));
				}
				System.out.println("Le language par défaut a bien été modifie");
			} catch (Exception e) {
				System.out.println("Le language par défaut n'a pas pu être modifie");
			}
		} else {
			System.out.println("Le language par defaut est correct - aucun changement apporte");
		}
	}

	public void login(String login, String password) {
		this.driver.findElement(INPUT_LOGIN).sendKeys(login);
		this.driver.findElement(INPUT_PASSWORD).sendKeys(password);
		this.driver.findElement(BUTTON_CONNECTION).click();
	}
}
