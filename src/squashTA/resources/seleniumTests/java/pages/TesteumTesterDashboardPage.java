package squashTA.resources.seleniumTests.java.pages;

import org.openqa.selenium.By;
import squashTA.resources.seleniumTests.java.utils.AbstractPage;
import squashTA.resources.seleniumTests.java.utils.AbstractTest;

public class TesteumTesterDashboardPage extends AbstractPage {
	public static final By HI_USER_TEXT_FIELD = By.xpath("//div[text()='Hi ']");
	public static final By WALLET_TITLE = By.xpath("//span[text()='Mon Wallet']");
	private static final By CHECK_ELEMENT_1 = WALLET_TITLE;
	private static final By CHECK_ELEMENT_2 = HI_USER_TEXT_FIELD;

	public TesteumTesterDashboardPage(AbstractTest test) { super(test); }

	public TesteumTesterDashboardPage(AbstractPage page) {
		super(page);
	}

	@Override
	protected void assertPage() {
		this.assertElementDisplayed(CHECK_ELEMENT_1);
		this.assertElementDisplayed(CHECK_ELEMENT_2);
	}
}
