/**
 * Created by matth on 15/10/2016.
 */

package squashTA.squashConfig;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class SquashConfig {
    public String baseUrl = "http://ec2-13-211-91-195.ap-southeast-2.compute.amazonaws.com:8080/";  //System.getenv("baseUrl");
    public String browser = "chrome";                  //System.getenv("browser");
    public String isRemote = "false";                   //System.getenv("isRemote");
    public FirefoxBinary binary = new FirefoxBinary(new File("C:/Program Files (x86)/Mozilla Firefox/firefox.exe"));
    public FirefoxProfile profile = new FirefoxProfile();
    public WebDriver driver;

    public SquashConfig() {
        if (browser == "firefox") {
            if(isRemote == "true"){
                System.out.println("Ouverture d'un navigateur distant.");
                DesiredCapabilities capability = DesiredCapabilities.firefox();
                capability.setPlatform(Platform.WINDOWS);
                capability.setBrowserName("firefox");
                capability.setVersion("20");
                try {
                    driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Ouverture d'un navigateur local.");
                driver = new FirefoxDriver(binary, profile);
            }
        } else if(browser == "chrome") {
            System.out.println("Ouverture d'un navigateur local.");
            System.setProperty("webdriver.chrome.driver", "webdrivers/chromedriver.exe");
            driver = new ChromeDriver();
        } else {
            System.out.println("Navigateur non compatible");
        }
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }
}
