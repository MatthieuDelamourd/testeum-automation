########################################
#                                      #
#  Designed by HIGHTEST - 15/10/2016   #
#                                      #
########################################

Ce repo contient le point de départ d'un projet de test d'application
web basé sur les outils squashTA et selenium.

HOW TO :
1 - Cloner le repo
2 - Importer le projet cloné dans l'IDE en tant que projet maven
3 - Dans le fichier src/squashTA/resources/selenium/java/Test1_chrome remplacer la valeur de
    baseUrl "http://www.hightest.nc/" par l'url de l'appli à tester
4 - Répéter l'étape 3 dans le fichier src/squashTA//sample-target.properties
5 - Si des startup/cleanup doivent être effectués dans une base données, saisir
les paramètres de la base dans le fichier src/squashTA/sample_db.properties
6 - Créer votre projet !

NOTE : Seuls chrome et firefox sont actuellement supportés